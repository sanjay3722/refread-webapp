import { Component, OnInit } from '@angular/core';
import { MockdataService } from '../../core/services/mockdata.service';

@Component({
  selector: 'refread-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  result: any;
  constructor(
    private mockdataservice: MockdataService
  ) { }

  ngOnInit(): void {
    this.getMockData();
  }

  getMockData() {
    return this.mockdataservice.mockData().subscribe(
      resData => {
        this.result = resData
        console.log(this.result);
      }
    )
  }
}
