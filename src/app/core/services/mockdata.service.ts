import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API } from '../../shared/utils/constant/api-constant';

@Injectable({
  providedIn: 'root'
})

export class MockdataService {
  constructor(
    private http: HttpClient
  ) { }

  mockData() {
    return this.http.get(API.MOCKDATA);
  }
}
