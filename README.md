# README #

This README would normally document whatever steps are necessary to get your application up and running.

This project is on Angular 9.

### How do I get set up project for Frontend Developers ?

1. Clone this project in your local.
2. Go inside your project folder (refread-webapp).
3. Run npm install
4. Run ng serve -o
5. open http://localhost:4200


### How do I run mockup json server to use mock api for Frontend Developers?

1. npm run json:server 
2. open http://localhost:3000